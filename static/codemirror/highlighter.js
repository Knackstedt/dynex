// Highlighter module for regex matching.
// Maintained by Andrew.Knackstedt

// Helpful link:
// https://codemirror.net/doc/manual.html#modeapi

(function(mod) {
    mod(CodeMirror);
})(function(CodeMirror) {
`use strict`;

CodeMirror.defineMode(`highlighter`, function() {
  function eatStandard(stream, pos){
    // All rules below this line are ONLY applied to non-match characters.
    if(stream.current() == ' ')
      return 'd space ' + pos;
    if(stream.current() == '\t')
        return 'd tab ' + pos;
    return 'd'; //Should not be separated by each character.
  }
  return {
    blankLine: function(state){
      state.position++;
    },

    startState: function() {
      console.log('Re-drawing the highlights.');

      // Populate an index array of the classes to attach to positions.
      // (Time Benchmark >2ms (Chrome))
      let matches = regexMatches;
      let matchCords = [];
      for(var j = 0; j < matches.length; j++){
        for(var i = 0; i < matches[j].length; i++){
          let pos = matches[j].index + i;
          matchCords[pos+1] = [`match pos-${matches[j].index} length-${matches[j].length} matchnumber-${j} ${j % 2 == 0 ? 'match-alt' : ''}`];
          for(var k = 0; k < matches[j].captures.length; k++){
            let capture = matches[j].captures[k];

            if(capture.index <= pos && capture.index+capture.length>pos)
              matchCords[pos+1].push(`cg-${capture.captureNumber} cg-${capture.captureNumber}-pos-${capture.index} cg-${capture.captureNumber}-length-${capture.length}`);
          }
        }
      }
      // console.log(matchCords);
      return {
        position: 0,
        nextMatch: regexMatches[0],
        matchNum: 0,
        captureNum: 0,
        matchCords: matchCords,
      };
    },
    token: function(stream, state) {
      // Track the stream's index. There should be no need to modify this statement.
      if(stream.eat(/./)){ state.position++; }

      // Eat a match.
      if(state.matchCords[state.position]){
        let sPos = state.position;
        if(stream.eol()){ state.position++; }
        return state.matchCords[sPos].join(' ');
      }
      if(stream.eol()){ state.position++; }

      return eatStandard(stream, state.position);
    },
  };
});

CodeMirror.defineMIME(`text/highlighter`, `highlighter`);
});
