// Regex parser for CodeMirror modes.
// Maintained by Andrew.Knackstedt

// Helpful link:
// https://codemirror.net/doc/manual.html#modeapi

(function(mod) {
    mod(CodeMirror);
})(function(CodeMirror) {
`use strict`;

const posixIdentifiers = [
  'alpha',
  'alnum',
  'ascii',
  'blank',
  'cntrl',
  'digit',
  'graph',
  'lower',
  'print',
  'punct',
  'space',
  'upper',
  'word',
  'xdigit'
];

CodeMirror.defineMode(`regex`, function() {
  // Eats stream for Literals and POSIX identifiers. WILL stop the BracketMode.
  function eatLiterals(stream, state){
    // Eat POSIX matches:
    if(stream.eat('[')){
      if(stream.eat(':')){
        // Eat the entire contents of the POSIX group.
        stream.eatWhile(/[^:]/);
        let streamVal = stream.current().slice(2);
        let matched = false;

        // Check if known POSIX match.
        matched = posixIdentifiers.find(function(e){return streamVal == e;});

        if(matched && stream.eat(':') && stream.eat(']')){// Good POSIX identifier
          return state.activeModes.literalGroup == 'inverse' ? `posix posix-inv` : `posix`;
        }
        else{// Bad POSIX identifier
          stream.eat(':'); //Try to eat the ending.
          stream.eat(']'); //Try to eat the ending.
          return `invalid-syntax-posix-invalidIdentifier`;
        }
      }
      // Happens when this isn't POSIX. This means it's just a standard '[' char.
      return state.activeModes.literalGroup == 'inverse' ? 'literal literal-inv' : 'literal';
    }

    // Eat Escaped characters and highlight invalid.
    if(stream.eat(`\\`)){
      if(stream.eat(/[xXuU]/)){
        // Eat up to 4 times.
        stream.match(/[a-fA-F0-9]/);
        stream.match(/[a-fA-F0-9]/);
        stream.match(/[a-fA-F0-9]/);
        stream.match(/[a-fA-F0-9]/);

        // If a dash is found after a unicode sequence in a literal block.
        if(stream.eat(/-/)){

          // If we see the start of escaping the sequence.
          if(stream.eat(`\\`)){

            // We found a proper match...
            if(stream.eat(/[xXuU]/)){
              if(stream.eat(/[a-fA-F0-9]/)){
                stream.eat(/[a-fA-F0-9]/);
                stream.eat(/[a-fA-F0-9]/);
                stream.eat(/[a-fA-F0-9]/);
                return `unicode-range`;
              }
              return `invalid-syntax-unicodeRange`;
            }
            stream.backUp(1);
          }
          stream.backUp(1);
          // stream.next();
          return `unicode-escaped`;
        }
        return `unicode-escaped`;
      }
      if(state.mode == 'renaming' && stream.eat(/\{|\}/)){
        return `invalid-dtRule-renaming-curlyBraces`;
      }
      if(stream.eat(/[^a-zA-Z]/)){
        return `unicode-escaped`;
      }

      // stream.next();
      var digitPairs = [
        [`d`, `posix-digit`],
        [`D`, `not-posix-digit`],
        [`b`, `posix-bound`],
        [`B`, `not-posix-bound`],
        [`h`, `posix-blank`],
        [`H`, `not-posix-blank`],
        [`w`, `posix-word`],
        [`W`, `not-posix-word`],
        [`s`, `posix-space`],
        [`S`, `not-posix-space`],
      ];
      for(var i = 0; i < digitPairs.length; i++){
        if(stream.eat(digitPairs[i][0][0])){
          return formatClasses(digitPairs[i][1], state);
        }
      }
    }

    // Character Ranges
    if(stream.eat(/[^-]/)){
      if(stream.eat('-')){

        // In this case, match the dash as a character.
        if(stream.peek() == `]`){
          return state.activeModes.literalGroup == 'inverse' ? 'literal-inv' : 'literal';
        }

        stream.eat(/./); //Eat the other end of the range.
        return state.activeModes.literalGroup == 'inverse' ? 'range range-inv': 'range';
      }
      else{
        // Undo the advance from the if statement since no valid matches made.
        stream.backUp(1);
      }
    }

    // End of Literal Group
    if(stream.eat(`]`)){
      // Done matching.
      state.activeModes.literalGroup = false;
      return 'literal-end';
    }
    // Default Behaviour
    stream.eat(/./); //Advance the stream on any character.
    return state.activeModes.literalGroup == 'inverse' ? 'literal-inv' : 'literal';
  }

  // Eats the quantifier and returns if it's possessive or lazy.
  function posOrLazy(stream, state, qClass){
    if(stream.eat(`+`)){
      if(stream.eat(/[+*?{]/))
        return `invalid-syntax-quantifier`;

      return `${qClass}-possessive`;
    }
    if(stream.eat(`?`)){
      if(qClass == 'zero-or-one')
        return `invalid-syntax-quantifier`;
      if(stream.eat(/[+*?{]/))
        return `invalid-syntax-quantifier`;

      return `${qClass}-lazy`;
    }
    if(stream.eat(/[*{]/)){
      return `invalid-syntax-quantifier`;
    }

    // If inside of a capture group, check for greedy quantifiers. These are disallowed.
    if(state.cgDepth >= 1 && state.mode.match(/requestAttributeExtraction|renaming|conditional/) && stream.eat(/[^+?]/)){
      stream.backUp(1);
      return `invalid-dtRule-captureGroup-greedyComponent`;
    }
    // Returns default (single char quantifier).
    return qClass;
  }

  function formatClasses(classes, state){
    return `tt ${classes} ${state.counter++}`;
  }
  return {
    startState: function() {
      return {
        mode: document.getElementById(`regex-mode`).children[document.getElementById(`regex-mode`).selectedIndex].value,
        activeModes:{
          numericRange: false,
          numericRangeLower: null,
          numericRangeClosed: false,
          numericRangeSingle: false,
          literalGroup: false,
        },
        cgDepth: 0,
        cgNum: 0,
        counter: 0
      };
    },
    token: function(stream, state) {
      //When in a literal match group, use dedicated function.
      if(state.activeModes.literalGroup) {
        return formatClasses(eatLiterals(stream, state), state);
      }

      // When in a {1,3} group, eat and color items.
      if(state.activeModes.numericRange){
        if(stream.eat('}')){
          state.activeModes.numericRange = false;
          state.activeModes.numericRangeLower = null;
          state.activeModes.numericRangeClosed = true;

          return formatClasses(`numericrange-end`, state);
        }

        if(stream.eat(',')){ //{,20}
          state.activeModes.numericRangeLower = -1;
          return formatClasses(`numericrange-delim`, state);
        }

        if(state.activeModes.numericRangeLower){
          if(stream.eatWhile(/\d/)){
            // If the number is higher on the left, is error.
            if(state.activeModes.numericRangeLower > parseInt(stream.current())){
              stream.eat(/./);
              return formatClasses(`invalid-syntax-numericRange-higherLeft`, state);
            }
            return formatClasses(`numericrange-num numericrange-upper`, state);
          }
        }
        else if(stream.eat(/\d/)){ //{20} {1,}
          stream.eatWhile(/\d/);
          if(stream.peek() == '}'){
            state.activeModes.numericRangeSingleValue = true;
            return formatClasses(`numericrange-num numericrange-lower`, state);
          }

          state.activeModes.numericRangeLower = parseInt(stream.current());
          return formatClasses(`numericrange-num numericrange-lower`, state);
        }

        // Consume any invalid characters in a numeric range.
        stream.eat(/./);
        return formatClasses(`invalid-syntax-numericRange-invalidSymbol`, state);
      }

      // When the last matched item was a number range, try to eat a + or ?.
      if(state.activeModes.numericRangeClosed){
        state.activeModes.numericRangeClosed = false;

        // Check for invalid quantifier characters.
        if(stream.eat('?'))
          return formatClasses(`numericrange-lazy ${state.activeModes.numericRangeSingleValue ? 'invalid-syntax-numericRange-extraQuantifier' :''}`, state);
        if(stream.eat('+'))
          return formatClasses(`numericrange-possessive ${state.activeModes.numericRangeSingleValue ? 'invalid-syntax-numericRange-extraQuantifier' :''}`, state);
        if(stream.eat('*'))
          return formatClasses(`invalid-syntax-numericRange-extraQuantifier`, state);
      }

      // If we are in a capture group ()
      if(state.cgDepth>0){
        // Special capture groups:
        stream.backUp(1);
        let lastChar = stream.next();
        let isOpt = stream.eat(`?`);
        if(isOpt && (lastChar == '(')){
          if(state.mode == 'default' && stream.eat('(')){
            // ???
          }
          // If we are running conditional mode, detect non-atomiccapture groups.
          if(state.mode == 'conditional'){
            if(stream.eat(/[?]/) && stream.eat(/[>]/))
              return formatClasses(`captureGroup atomic captureGroup`, state);
            // ANY group, lookahead or other that is not atomic is not allowed.
            return formatClasses(`invalid-dtRule-captureGroup-nonAtomic captureGroup`, state);
          }

          //MUST be defined. Otherwise is bad match.
          switch(stream.next()){
            case ":":{
              return formatClasses(`captureGroup non-capture`, state);
            }
            case "=":{
              if(state.mode == 'renaming')
                return formatClasses(`invalid-dtRule-captureGroup-renaming-lookahead captureGroup`, state);
              return formatClasses(`captureGroup lookahead`, state);
            }
            case "!":{
              return formatClasses(`captureGroup lookahead-inv`, state);
            }
            case ">":{
              return formatClasses(`captureGroup atomic`, state);
            }
            case "<":{
              if(stream.eat(`=`))
                return formatClasses(`captureGroup lookbehind`, state);
              else if(stream.eat(`!`))
                return formatClasses(`captureGroup lookbehind-inv`, state);
            }
            case "#":{
              // ???
            }
            case "P":{
              // Dynatrace doesn't support named groups.
              if(state.mode != 'default'){
                stream.eat('<');
                stream.eatWhile(/[A-Za-z0-9]/);
                stream.eat('>');
                return formatClasses(`captureGroup invalid-dtRule-captureGroup-namedGroup`, state);
              }
              if(stream.eat('<') && stream.eat(/[A-Za-z0-9]/)){
                stream.eatWhile(/[A-Za-z0-9]/); //Eat the entire identifier...
                if(stream.eat('>')){
                  return formatClasses(`captureGroup namedGroup`, state);
                }
              }
              return formatClasses(`captureGroup invalid-syntax-captureGroup-namedGroup`, state);
            }
            default: {
              var last = `?`;
              stream.eatWhile(/(?=[^\\])[^)]/);
              stream.eat(`)`);
              return formatClasses(`captureGroup invalid-syntax-captureGroup-operator`, state);
            }
          }
        }
        else if(isOpt){
          stream.backUp(1);
        }

        // Close the Capture Group.
        if(stream.eat(')')){
          state.cgDepth--;
          if(state.mode.match(/naming/)){
            if(stream.eat(/[+*?{]/))
              return formatClasses(`invalid-dtRule-captureGroup-groupQuantifier captureGroup-end cgNum-${state.cgNum}`, state);
          }
          return formatClasses(`captureGroup-end cgNum-${state.cgNum}`, state);
        }
        // If is a conditional CG and NOT atomic, it's invalid.
        if(state.mode == 'conditional'){
          stream.eatWhile(/[^)]/);
          return formatClasses(`invalid-dtRule-captureGroup-nonAtomic captureGroup-start`, state);
        }
      }

      if(state.namedgroup){
        if(stream.eat('>')){
          state.namedgroup = false;
          return formatClasses(`namedgroup-delim`, state);
        }

        stream.eatWhile(/[^>]/);
        state.namedgroup = true;
        return formatClasses(`namedgroup`, state);
      }

      //Dynatrace shows things like {1,35} as too expensive. How should we show this as invalid?
      switch(stream.next()){ //Stream.next advances the stream. stream.peek does not.
        case `{`: {
          state.activeModes.numericRangeSingleValue = false;
          if(state.mode == 'renaming'){
            stream.eatWhile(/[^}]/);
            stream.eat('}');
            return formatClasses(`invalid-dtRule-renaming-curlyBraces`, state);
          }
          state.activeModes.numericRange = true;

          // Check for invalid symbols.
          if(stream.eat(/[^\d\-\}\,\{]/))
            return formatClasses(`invalid-syntax-numericRange-invalidSymbol`, state);

          return formatClasses(`numericrange-start`, state);
        }
        case `}`:{
          console.error('Error: Closing Curly Brace not matched.');
          return formatClasses(`invalid-syntax-numericRange-extraClosingBrace`, state);
        }
        case `[`: {
          if(stream.eat('^'))
            state.activeModes.literalGroup = 'inverse';
          else
            state.activeModes.literalGroup = true;

          return formatClasses(`literal-start`, state);
        }
        case `]`: {
          console.error('Error: Closing Bracket not matched.');
          state.bracketMode = false;
          return formatClasses(`invalid-syntax-literalGroup-extraClosingBracket`, state);
        }
        case `\\`: {
          if(state.mode == 'renaming' && stream.eat(/[{}]/))
            return formatClasses(`invalid-dtRule-renaming-curlyBraces`, state);
          if(stream.eat(`\\`)){
            return formatClasses(`escaped`, state);
          }
          if(stream.eat(/[xXuU]/)){
            if(stream.eat(/[0-9A-Fa-f]/)){
              stream.eat(/[0-9A-Fa-f]/);
              stream.eat(/[0-9A-Fa-f]/);
              stream.eat(/[0-9A-Fa-f]/);
              return formatClasses(`unicode-escaped`, state);
            }
            // stream.next();
            return formatClasses(`invalid-syntax-unicode-emptyValue`, state);
          }
          if(stream.eat(/[aA]/)){
            return formatClasses(`string-start`, state);
          }

          if(stream.eat('k')){
            if(state.mode != 'default'){ //Dynatrace doesn't support this.
              stream.eatWhile(/[^>]/);
              return formatClasses(`invalid-dtRule-captureGroup-namedGroup`, state);
            }

            stream.eat('<');
            state.namedgroup = true;
            return formatClasses(`namedgroup-delim`, state);
          }
          if(stream.eat(/[0-9]/)){
            if(state.mode != 'default'){ //Dynatrace doesn't support this.
              return formatClasses(`invalid-dtRule-backreference`, state);
            }
            return formatClasses(`backreference`, state);
          }

          var digitPairs = [
            [`d`, `posix-digit`],
            [`D`, `not-posix-digit`],
            [`b`, `posix-bound`],
            [`B`, `not-posix-bound`],
            [`h`, `posix-blank`],
            [`H`, `not-posix-blank`],
            [`w`, `posix-word`],
            [`W`, `not-posix-word`],
            [`s`, `posix-space`],
            [`S`, `not-posix-space`],
          ];
          for(var i = 0; i < digitPairs.length; i++){
            if(stream.eat(digitPairs[i][0][0])){
              return formatClasses(digitPairs[i][1], state);
            }
          }
          if(stream.eat(/\W/)){
            // This should match ]...
            return formatClasses(`escaped-symbol`, state);
          }
          // All stream content that proceeds past this is invalid in Dynatrace.

          // Eat the entire \p{Ascii} sequence
          if(stream.eat(/[pP]/)){
            if (stream.eat(`{`)) {
              stream.eatWhile(/[^}]/);
              stream.eat(`}`);
            }
            if(state.mode == 'default')
              return formatClasses(`posix`, state);
            else
              return formatClasses(`invalid-syntax-escapedCharacter`, state);
          }

          // Eat the Backreference and it`s name.
          // TODO: Check that this can be executed. If not, kill it.
          if(stream.eat(/[kK0-9]/)){
            if (stream.eat(`<`)) {
              stream.eatWhile(/[^>]/);
              stream.eat(`>`);
            }
            if(state.mode != 'default')
              return formatClasses(`invalid-dtRule-backreference`, state);
            else
              return formatClasses(`backreference`, state);
          }

          if(stream.eat('G')){
            if(state.mode != 'default')
              return formatClasses(`invalid-dtRule-previousMatch`, state);
            else
              return formatClasses(`previousMatch`, state);
          }
          if(stream.eat('g')){
            if(stream.eat('{')){
              stream.eatWhile(/[^}]/);
              stream.eat('}');
              if(state.mode == 'default')
                return formatClasses(`match-group`, state);
              else
                return formatClasses(`invalid-dtRule-groupReference`, state);
            }
            if(stream.eat(/\d/)){
              if(state.mode == 'default')
                return formatClasses(`match-group`, state);
              else
                return formatClasses(`invalid-dtRule-groupReference`, state);
            }
            return formatClasses(`invalid-syntax-groupReference`, state);
          }
          // Catch all for any invalid escaped characters.
          stream.next();
          if(state.mode != 'default')
            return formatClasses(`invalid-dtRule-unknownEscape`, state);
          else
            return formatClasses(`escaped`, state);
        }
        case `(`: {
          state.cgDepth++;
          state.cgNum++;
          if(state.mode == 'renaming' && state.cgNum >= 2) //Yes, DT doesn't care if they are 'technically not' capture groups.
            return formatClasses(`invalid-dtRule-captureGroup-quantity captureGroup-start `, state);
          return formatClasses(`captureGroup-start cgNum-${state.cgNum}`, state);
        }
        case `)`: {
          return formatClasses('invalid-syntax-captureGroup-closingParenthesis', state);
        }
        case `$`:{
          return formatClasses(`end-of-string`, state);
        }
        case `^`:{
          return formatClasses(`start-of-string`, state);
        }
        case `|`:{
          return formatClasses(`alternator`, state);
        }
        case `+`: {
          return formatClasses(posOrLazy(stream, state, 'one-or-more'), state);
        }
        case `*`: {
          return formatClasses(posOrLazy(stream, state, 'zero-or-more'), state);
        }
        case `?`: {
          return formatClasses(posOrLazy(stream, state, 'zero-or-one'), state);
        }
        case ` `: {
          return formatClasses(`space`, state);
        }
        case `\t`: {
          return formatClasses(`tab`, state);
        }
        case '.':{
          return formatClasses(`any`, state);
        }
        default: {
          // Put quantifiers on standard literal characters.
          if(stream.eat(`*`)){
            return formatClasses(posOrLazy(stream, state, 'zero-or-more'), state);
          }
          if(stream.eat(`+`)){
            return formatClasses(posOrLazy(stream, state, 'one-or-more'), state);
          }
        }
      }
      return 'tt default';
    }
  };
});

CodeMirror.defineMIME(`text/regex`, `regex`);
});
