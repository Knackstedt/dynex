// Maintained by Andrew.Knackstedt
"use strict";

// Log the server's version to the chrome Devtools.
axios.get('/version').then(function(res){console.log(res.data);});

const preFillPairs = [ //Just a simple datastore for the samples. Probably could use a json file, sue me.
  ['username', 'username=([^;]++)', 'firstName=Morgan;lastName=Freeman;username=TheRealMorgan;email=freemorgans@fake.com;address=100 fake street, Springfield IL, 99999'],
  ['email', 'email=([A-Za-z0-9._\\-\'"@*]++)', 'firstName=Morgan;lastName=Freeman;username=TheRealMorgan;email=freemorgans@fake.com;address=100 fake street, Springfield IL, 99999;'],
  ['price', '\\$\\d+,?\\d+,?\\d+\\.?\\d+', 'the price is $1,000.00. How would you like to pay that? '],
  ['tag', 'tags=([^&]++)', 'path=/example/folder/goes/here&meta=1111&tags=Orange, Lemon, Pie, Cream, Food, Baked Goods&price=$11.99'],
];

function preFill(id){
  for(var i = 0; i < preFillPairs.length; i++){
    if(preFillPairs[i][0] == id){
      inputEditor.setValue(preFillPairs[i][2]);
      regexEditor.setValue(preFillPairs[i][1]);
    }
  }
}

function switchSidebarTab(tabName, button){
  let tabs = document.querySelectorAll('.tab-contents>div');
  for(var i = 0; i < tabs.length; i++){
    tabs[i].classList.add('hide');
  }

  let buttons = document.querySelectorAll('.tab-selectors > div');
  for(var i = 0; i < buttons.length; i++){
    buttons[i].classList.remove('selected');
  }

  document.querySelector(tabName).classList.remove('hide');
  button.classList.add('selected');
}

function toast(message, severity){
  new Noty({
    theme: 'metroui',
    timeout: 3000,
    type: severity,
    closeWith: 'button',
    text: message
  }).show();
}

// Show the user how long the regex took to evaluate.
function markDuration(timeMs){
  let timeFormatted = timeMs.toFixed(0);
  document.getElementById('match-timer-value').innerText = timeFormatted;
  // console.log(timeFormatted);
}

let bugImagesLoaded = false;
function loadBugImages(){
  u('.bug-report').nodes[0].classList.add("show");
  if(!bugImagesLoaded){
    let lazyimages = document.querySelectorAll('.bug-report img');
    for(var i = 0; i < lazyimages.length; i++){
      lazyimages[i].setAttribute('src', lazyimages[i].getAttribute('data-src'));
    }
    bugImagesLoaded = true;
  }
}

// Escape the HTML characters for the message.
function escapeHtml(raw) {
  if(!raw)
    return 'Error. Please create and Issue and or yell at the developer.';

  return raw.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
}

// Update results when recieved match results from server.
async function updateResults(){
  let comboBox = document.getElementById('regex-mode');
  let mode = comboBox[comboBox.selectedIndex].getAttribute('cfg');

  let resultList = '';
  let resultDisplay = document.getElementById('matchResults');
  if(mode && mode != 'default'){
    regexMatches.forEach(function(item, i){resultList += `<li ${i==0 ? 'class="mainResult"':''}><span class='matchId'>Match ${i+1}: </span><span class='value'>${escapeHtml(item.firstCapture || item.text)}</span></li>`;});
  }
  else{
    // Standard Mode
    regexMatches.forEach(function(item, i){resultList += `<li><span class='matchId'>Match ${i+1}: </span><span class='value'>${escapeHtml(item.text)}</span></li>`;});
  }
  resultDisplay.innerHTML = resultList;
}



tooltipMouseHandlers.highlightCaptureGroups = function(e){
  if(e.type == 'mouseover'){
    let classes = e.target.parentElement.classList.value;

    // console.log(e.target.parentElement);
    let cgNum = classes.match(/cgNum-(\d+)/)[1];
    let matches = document.querySelectorAll(`.cm-cg-${cgNum}`);
    matches.forEach(function(e){e.classList.add('hover');});
  }
  else if(e.type == 'mouseout'){
    let classes = e.target.parentElement.classList.value;

    let cgNum = classes.match(/cgNum-(\d+)/)[1];

    let matches = document.querySelectorAll(`.cm-cg-${cgNum}`);
    matches.forEach(function(e){e.classList.remove('hover');});
  }
};

regexEditor.setValue('and (\\w++)');

inputEditor.setValue(`DynEx is a Regex tester that highlights errors in regex that Dynatrace would normally disallow.
By setting the regex mode to the same type of use you need to use a regex for, DynEx will automatically highlight and explain the details about any issue in a regex.
With extraction rules, Dynatrace will only utilize the first capture group, and throw warnings if you try to create multiple capture groups.
Other rules may have rule-specific configurations, which will all be handled internally and highlight issues in the regex.

If any errors are missed, Please create an issue on GitLab and we can implement the logic to detect it!`);
