// Maintained by Andrew.Knackstedt

const posixIdentifiers = [ 'alpha', 'alnum', 'ascii', 'blank', 'cntrl', 'digit', 'graph', 'lower', 'print', 'punct', 'space', 'upper', 'word', 'xdigit' ];
const multiTokenClasses = /cm-((literal|captureGroup)-?(start|end)?|numericrange-(start|end|delim|lazy|possessive)|(range|literal)-?(inv-)?(start|end)?)/i;


function crawlDomSiblings(startElement, leftCondition, rightCondition){
  let l = startElement.previousSibling, r = startElement.nextSibling;
  let domItems = [startElement];
  while(l && leftCondition && leftCondition(l)){
    domItems.unshift(l);
    l = l.previousSibling;
  }
  while(r && rightCondition && rightCondition(r)){
    domItems.push(r);
    r = r.nextSibling;
  }
  return domItems;
}

// Returns a list of all items in a capture group or literal series.
function getCaptureGroupTokens(startElement){
  let tokenList = [startElement];
  let currentElement = startElement;

  if(startElement.classList.contains('cm-literal-end')){
    return crawlDomSiblings(startElement, function(e){
      return e.classList.contains('cm-literal') && !e.classList.contains('cm-literal-end');
    });
  }
  else if(startElement.classList.contains('cm-literal-start')){
    return crawlDomSiblings(startElement, undefined, function(e){
      return e.classList.contains('cm-literal') && !e.classList.contains('cm-literal-start');
    });
  }
  else if(startElement.classList.contains('cm-literal') || startElement.classList.contains('cm-range')) {
    return crawlDomSiblings(startElement, function(e){
      return (e.classList.contains('cm-literal') || e.classList.contains('cm-range')) && !e.classList.contains('cm-literal-end');
    }, function(e){
      return (e.classList.contains('cm-literal') || e.classList.contains('cm-range')) && !e.classList.contains('cm-literal-start');
    });
  }
  else if(startElement.classList.contains('cm-literal-inv') || startElement.classList.contains('cm-range-inv')) {
    return crawlDomSiblings(startElement, function(e){
      return (e.classList.contains('cm-literal-inv') || e.classList.contains('cm-range-inv')) && !e.classList.contains('cm-literal-end');
    }, function(e){
      return (e.classList.contains('cm-literal-inv') || e.classList.contains('cm-range-inv')) && !e.classList.contains('cm-literal-start');
    });
  }

  else if(startElement.classList.contains('cm-captureGroup-end')){
    let cgNum = -1;
    while(currentElement && !currentElement.classList.contains('cm-captureGroup-start') || cgNum != 0){
      if(currentElement.classList.contains('cm-captureGroup-end'))
        cgNum++;
      if(currentElement.classList.contains('cm-captureGroup-start'))
        cgNum--;
      currentElement = currentElement.previousSibling;
      tokenList.unshift(currentElement);
    }
    tokenList.pop();
    tokenList.shift();

    // Check if there is a CaptureGroup option present, and skip adding it to the results.
    if(tokenList[0].classList.value.match(/namedGroup|look|atomic|non-capture/))
      tokenList.shift();
  }
  else if(startElement.classList.contains('cm-captureGroup-start')){
    let cgNum = -1;
    while(currentElement && !currentElement.classList.contains('cm-captureGroup-end') || cgNum != 0){
      if(currentElement.classList.contains('cm-captureGroup-start'))
        cgNum++;
      if(currentElement.classList.contains('cm-captureGroup-end'))
        cgNum--;
      currentElement = currentElement.nextSibling;
      tokenList.push(currentElement);
    }
    tokenList.pop();
    tokenList.shift();
  }
  else if(startElement.classList.contains('cm-captureGroup')){
    let leftToken = startElement.previousSibling;
    let rightToken = startElement.nextSibling;

    let leftCount = 0;
    while(!leftToken.classList.contains('cm-captureGroup-start') || leftCount != 0){
      // Account for capture group nesting.
      if(leftToken.classList.contains('cm-captureGroup-end'))
        leftCount++;
      if(leftToken.classList.contains('cm-captureGroup-start'))
        leftCount--;

      tokenList.unshift(leftToken);
      leftToken = leftToken.previousSibling;
    }
    // tokenList.unshift(leftToken);
    tokenList.shift();

    let rightCount = 0;
    while(rightToken && !rightToken.classList.contains('cm-captureGroup-end') || rightCount != 0){
      // Account for capture group nesting.
      if(rightToken.classList.contains('cm-captureGroup-start'))
        rightCount++;
      if(rightToken.classList.contains('cm-captureGroup-end'))
        rightCount--;

      tokenList.push(rightToken);
      rightToken = rightToken.nextSibling;
    }
    // tokenList.push(rightToken);
  }

  else if(startElement.classList.contains('cm-numericrange-start')){
    while(currentElement && !currentElement.classList.contains('cm-numericrange-end')){
      currentElement = currentElement.nextSibling;
      tokenList.push(currentElement);
    }
    if(currentElement.nextSibling && currentElement.nextSibling.classList.value.match(/cm-numericrange-(possessive|lazy)/))
      tokenList.push(currentElement.nextSibling);
  }
  else if(startElement.classList.contains('cm-numericrange-end')){
    if(currentElement.nextSibling && currentElement.nextSibling.classList.value.match(/cm-numericrange-(possessive|lazy)/))
      tokenList.push(currentElement.nextSibling);

    while(currentElement && !currentElement.classList.contains('cm-numericrange-start')){
      currentElement = currentElement.previousSibling;
      tokenList.unshift(currentElement);
    }
  }
  else if(startElement.classList.contains('cm-numericrange-delim')){
    let leftToken = startElement.previousSibling;
    let rightToken = startElement.nextSibling;
    while(!leftToken.classList.contains('cm-numericrange-start')){
      tokenList.unshift(leftToken);
      leftToken = leftToken.previousSibling;
    }
    tokenList.unshift(leftToken);
    while(!rightToken.classList.contains('cm-numericrange-end')){
      tokenList.push(rightToken);
      rightToken = rightToken.nextSibling;
    }
    tokenList.push(rightToken);

    if(rightToken.nextSibling && rightToken.nextSibling.classList.value.match(/cm-numericrange-(possessive|lazy)/))
      tokenList.push(rightToken.nextSibling);

  }
  else if(startElement.classList.contains('cm-numericrange-possessive')||startElement.classList.contains('cm-numericrange-lazy')){
    while(currentElement && !currentElement.classList.contains('cm-numericrange-start')){
      currentElement = currentElement.previousSibling;
      tokenList.unshift(currentElement);
    }
  }
  else{
    throw {message: "Element does not have correct class!", details: startElement};
  }
  return tokenList;
}

function extractCaptureGroupLabel(e){
  let tokens = getCaptureGroupTokens(e);
  let innerRegex = '';
  tokens.forEach(function(e){innerRegex += e.innerText;});
  return innerRegex;
}

function getNumberRangeText(e){
  let items = crawlDomSiblings(e, function(e){
    return e.classList.value.match(/numericrange/);
  }, function(e){
    return e.classList.value.match(/numericrange/);
  });

  let numberString = '';

  let lower, upper, delim, lazy, possessive;

  // Load the item text onto the output string & set lower/upper values.
  items.forEach(function(e){
    if(e.classList.value.match(/numericrange-upper/))
      upper = e.innerText;
    if(e.classList.value.match(/numericrange-lower/))
      lower = e.innerText;
    if(e.classList.value.match(/numericrange-delim/))
      delim = e.innerText;
    numberString += e.innerText;
  });

  lazy = items[items.length-1].classList.value.match(/numericrange-lazy/);
  possessive = items[items.length-1].classList.value.match(/numericrange-possessive/);

  return {
    pre: `Quantifier`,
    post: `matches the preceding token ` +
    `${lower && delim? `at least ${lower} times` : ``} ` +
    `${lower && !delim? `exactly ${lower} times` : ``} ` +
    `${lower && upper ? `and` : ``} ` +
    `${upper ? `at most ${upper} times` : ``}` +
    `${lazy ? ', matching as few as possible' : ''}` +
    `${possessive ? ', matching as many tokens as possible without releasing any tokens' : ''}` +
    '.',
    cm: numberString
  };
}

// Create and add tooltip element.
var tooltip = document.createElement('span');
// tooltip.id = 'tt';
tooltip.classList.add('tt');
tooltip.innerHTML = `<span class='tt-pre'></span><span class='tt-mid'></span><span class='tt-post'></span>`;
let ttRx = CodeMirror(tooltip.querySelector('.tt-mid'), {
  mode: 'text/regex',
  scrollbarStyle: null,
  value: '',
  autoRefresh: true,
  readOnly: 'nocursor'
});

document.body.appendChild(tooltip);

let lastItems;
// Tooltip hover for regex details.

let regexTooltips = function(e){
  if(e.type === 'mouseover'){
    document.onmousemove = tooltipTracker;

    // Identify the elements that should be highlighted.
    if(e.target && e.target.classList.value.match(multiTokenClasses)){
      try{
        lastItems = getCaptureGroupTokens(e.target);
        lastItems.forEach(function(e){e.classList.add('hover');});
      }
      catch(ex){ console.log('Failure getting tooltip on', e, ex); }
    }
    else
      e.target.classList.add('hover');

    // Find & Apply the tooltip message.
    let helpMessage = messages[e.target.classList[1]];
    if(helpMessage){
      let tooltipData = typeof helpMessage == 'function' ? helpMessage(e.target) : helpMessage;
      if(typeof tooltipData == 'object'){
        tooltip.querySelector('.tt-pre').innerHTML = tooltipData.pre || '';
        tooltip.querySelector('.tt-post').innerHTML = tooltipData.post || '';
        ttRx.doc.setValue(tooltipData.cm);
        tooltip.querySelector('.tt-mid').style.opacity = 1;
      }
      else{
        tooltip.querySelector('.tt-pre').innerHTML = tooltipData;
        tooltip.querySelector('.tt-post').innerHTML = '';
        tooltip.querySelector('.tt-mid').style.opacity = 0;
      }
    }
    else{
      // This occurs if there was no match made.
      if(e.target) console.error(e.target.classList[1]);
      if(!e.target) console.warn('No target found!');

      tooltip.querySelector('.tt-pre').innerHTML = `Missing tooltip data!
      <br/>Please create an issue on GitLab.
      <br/>Class Data: ${e.target.classList}
      <hr/>You can find the gitlab link in the top-right corner.`;
      tooltip.querySelector('.tt-post').innerHTML = '';
    }
    tooltip.style.opacity = 1;
  }
  else { //MouseLeave -> hide the tooltip stuff.
    tooltip.style.opacity = '0';

    document.onmousemove = null;

    if(lastItems){
      lastItems.forEach(function(e){e.classList.remove('hover');});
      lastItems = null;
    }
    else
      e.target.classList.remove('hover');
  }
};
tooltipMouseHandlers.regexTooltips = regexTooltips;
function getNumberTitle(num){
  let sNum = num.toString();
  if(sNum.match(/1$/))
    return 'st';
  if(sNum.match(/2$/))
    return 'nd';
  if(sNum.match(/3$/))
    return 'rd';
  return 'th';
}

// This follows the mouse and moves the tooltip div box to follow what the cursor is following.
// Could do this with a static hover, this has had fewer bugs.
let tooltipTracker = function(e){
  let ttPos = tooltip.getBoundingClientRect();

  const minPadding = '5px';

  // Prevent the tooltip from clipping through the top side of the window.
  if(e.clientY - ttPos.height < 30){
    tooltip.style.top = (e.clientY + 25) + 'px';
  }
  else{
    tooltip.style.top = (e.clientY - (ttPos.height+25)) + 'px';
    tooltip.style.bottom = '';
  }
  // Clear left/right position.
  tooltip.style.right = null;
  tooltip.style.left = null;

  // Prevent the tooltip from clipping through the left side of the window.
  if(e.clientX - (ttPos.width/2) < 5){
    tooltip.style.left = minPadding;
  }
  // Prevent the tooltip from clipping though the right side of the window.
  else if((ttPos.width/2) + e.clientX > window.innerWidth-5){
    tooltip.style.right = minPadding;
  }
  else{ // Tooltip isn't clipped to left or right sides.
    tooltip.style.left = (e.clientX - (ttPos.width/2)) + 'px';
  }
};

// Tooltip hover for match/capture.
let showMatchDetails = function(e){
  if(e.type === 'mouseover'){
    document.onmousemove = tooltipTracker;
    // Calculate position for tooltip and highlight.
    var pos = e.target.getBoundingClientRect();

    let classes = e.target.classList.value;

    let text = e.target.innerText;
    let index = classes.match(/cm-pos-(\d+)/)[1];
    let matchNumber = classes.match(/cm-matchnumber-(\d+)/)[1];
    let length = classes.match(/cm-length-(\d+)/)[1];

    let cgNumber;
    let cgIndex;
    let cgLength;

    let isCapture = /cg/.test(classes);
    if(isCapture){
      cgNumber = classes.match(/cm-cg-(\d+)/)[1];
      cgIndex = classes.match(new RegExp(`cm-cg-${cgNumber}-pos-(\\d+)`))[1];
      cgLength = classes.match(new RegExp(`cm-cg-${cgNumber}-length-(\\d+)`))[1];
    }
    else{
      let l = e.target.previousSibling, r = e.target.nextSibling;
      while(l && l.classList.value.match(/cm-matchnumber-(\d+)/) && l.classList.value.match(/cm-matchnumber-(\d+)/)[1] == matchNumber){
        text = l.innerText + text;
        l = l.previousSibling;
      }
      while(r && r.classList.value.match(/cm-matchnumber-(\d+)/) && r.classList.value.match(/cm-matchnumber-(\d+)/)[1] == matchNumber){
        text += r.innerText;
        r = r.nextSibling;
      }
    }

    tooltip.querySelector('.tt-pre').innerHTML =
     `<span>Match <span>${(parseInt(matchNumber)+1)}</span></span>
      <hr/>
      <span>Group <span class='match-number'>${cgNumber ? cgNumber : 0}</span>: <span>${text}</span></span>
      <br/>
      <span>Index: <span class='match-data'>${cgIndex ? cgIndex : index}</span> length: <span class='match-data'>${cgLength ? cgLength : length}</span></span>`;
    tooltip.querySelector('.tt-mid').style.opacity = 0;
    tooltip.querySelector('.tt-post').innerHTML = '';

    let wholeMatch = document.querySelectorAll(`.cm-pos-${index}`);
    for(var i = 0; i < wholeMatch.length; i++){
      wholeMatch[i].classList.add('hover');
    }
    tooltip.style.opacity = 1;
  }
  else { //MouseLeave -> hide the tooltip stuff.
    tooltip.style.opacity = '0';
    document.onmousemove = null;

    let classes = e.target.classList.value;
    let index = classes.match(/cm-pos-(\d+)/)[1];

    let wholeMatch = document.querySelectorAll(`.cm-pos-${index}`);
    for(var i = 0; i < wholeMatch.length; i++){
      wholeMatch[i].classList.remove('hover');
    }
  }
};//);

tooltipMouseHandlers.showMatchDetails = showMatchDetails;
