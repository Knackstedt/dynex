// Maintained by Andrew.Knackstedt
"use strict";

let tsRunning = false;
function tryUpdateMatches(){
  if(!tsRunning){
    tsRunning = true;
    window.setTimeout(function(){
      tsRunning = false;
      getExpressionMatches();
    }, 250);
  }
}

function getExpressionMatches(){
  let rxStr = regexEditor.getValue();
  let rxEvalStr = inputEditor.getValue();

  let comboBox = document.getElementById('regex-mode');
  let mode = comboBox[comboBox.selectedIndex].getAttribute('value');

  // Limit data to roughky 8K.
  if(rxStr.length > 100 && mode != 'default'){
    toast("Your Regex is too long. Shorten it to less than 100 characters.", "error");
    return;
  }
  if(rxEvalStr.length > 8092){
    toast("Your Regex input evaluation string is too long. It must be less than 8,000 characters", "error");
    return;
  }
  if(rxStr.length == 0 || rxEvalStr.length == 0){
    regexMatches = [];
    return;
  }

  if(mode.match(/sessionPropertyCleanup/)) {
    // Must contain at least 1 capture group.
    if(document.querySelectorAll('.cm-cgNum-1').length == 0 || document.querySelectorAll('.cm-cgNum-2').length != 0){
      toast('Session and user action Property cleanup rules require <b>exactly</b> 1 capture group.', 'error');
    }
  }
  // else if (mode.match(//)){
  //   if(document.querySelectorAll('.cm-cgNum-1').length == 0 || document.querySelectorAll('.cm-cgNum-2').length != 0){
  //     toast('Session and user action Property cleanup rules require <b>exactly</b> 1 capture group.', 'error')
  //   }
  // }
  //Use the red highlighting from the lexer to determine if there are issues in the regex.
  var dtIssues = document.querySelectorAll('.cm-dt-invalid');
  var rxIssues = document.querySelectorAll('.cm-invalid');
  if(dtIssues.length > 0 || rxIssues.length > 0){
    return;
  }

  axios.post('/parser/pcre', {
    rx: rxStr,
    rxEval: rxEvalStr
  }).then(function (res) {
    if(res.data.responseCode){
      console.log("Failure: ", res.data.message);
      return;
    }

    let data = res.data.results;
    let duration = res.data.duration;

    regexMatches = [];

    try{
      let firstCapture = false;
      for(var i = 0; data && i < data.length; i++){
        // Find capture groups.
        let captures = [];
        for(var j = 1; j < data[i].length; j++){
          if(data[i][j] && data[i][j].match != ""){
            captures.push({
              index: data[i][j].start,
              length: data[i][j].end - data[i][j].start,
              captureNumber: j
            });
          }
        }
        regexMatches.push({
          index: data[i][0].start, //matched item index
          length: data[i][0].end - data[i][0].start, //matched item length
          text: data[i][0].match, //Entire match text
          firstCapture: data[i][1] && data[i][1].match,
          captures,
        });

      }
      if(!duration)
        debugger;
      markDuration(duration);
    }
    catch(ex){
      console.log(ex);
    }

    // Set the regex matches.
    // regexMatches = resData;

    // Re-start the highlighter with the new match data.
    inputEditor.setOption("mode", "text/highlighter");

    updateResults();
  })
  .catch(function (error) {
    console.log('Failed to get matches.');
    console.error(error);
  });
}

let tsDiagram = false;
// This is used to ensure the order of diagram generation. (Should use a timeout instead?)
function tryUpdateDiagram(){
  if(!tsDiagram){
    tsDiagram = true;
    window.setTimeout(function(){
      tsDiagram = false;
      getExpressionDiagram();
    }, 250);
  }
}

var conNo = 0;
var lastNo = 0;
async function getExpressionDiagram(){
  let regex = encodeURIComponent(regexEditor.getValue());

  // Skip if regex is empty.
  if(regex.length == 0){
    document.getElementById('rxr').innerHTML = '';
    return;
  }

  axios.get(`/diagram?regex=${regex}`)
  .then(function(response){
    if(response.status == 200)
      document.getElementById('rxr').innerHTML = response.data;
    else if(response.status == 299){
      document.getElementById('rxr').innerHTML = response.data.replace(/\\\\/g, '\\');
      toast("Invalid Regex Syntax.", 'warning');
    }
  });
}
