const messages = {
  // Invalid Syntax messages.
  'cm-invalid-syntax-unicodeRange': 'This Escaped Unicode Character range is invalid.',
  'cm-invalid-syntax-curlyBraces-empty': 'You cannot have an empty range statement. Please specify a number or range.',
  'cm-invalid-syntax-quantifier': 'This quantifier is invalid. Please remove.',
  'cm-invalid-syntax-numericRange-higherLeft': 'This Number Range is invalid. The left-hand side CANNOT be greater than the right-hand side.',
  'cm-invalid-syntax-numericRange-invalidSymbol': 'This symbol is not correct in the Numeric Range syntax.',
  'cm-invalid-syntax-numericRange-extraQuantifier': 'This quantifier is invalid. Please remove.',
  'cm-invalid-syntax-numericRange-extraClosingBrace': 'This closing curly brace is not matched or escaped to anything.',
  'cm-invalid-syntax-captureGroup-operator': 'This Operator is invalid for a Capture Group.',
  'cm-invalid-syntax-captureGroup-closingParenthesis': 'This closing parenthesis is not matched to anything.',
  'cm-invalid-syntax-captureGroup-namedGroup': 'This named group syntax is invalid.',
  'cm-invalid-syntax-literalGroup-extraClosingBracket': 'This closing bracket has no matching opening bracket.',
  'cm-invalid-syntax-unicode-emptyValue': 'This unicode escape sequence has no value.',
  'cm-invalid-syntax-escapedCharacter': 'This is an invalid escape sequence.',
  'cm-invalid-syntax-posix-invalidIdentifier': 'This is an unknown POSIX identifier.',

  // DT Rule Violation Messages.
  'cm-invalid-dtRule-renaming-curlyBraces': 'You cannot use Curly Braces in ANY capacity when making an entity naming rule.',
  'cm-invalid-dtRule-captureGroup-nonAtomic': 'Dynatrace enforced the use of Atomic groups here. Please append ">?" after the beginning of the group declaration.',
  'cm-invalid-dtRule-captureGroup-renaming-lookahead': 'Dynatrace does not support lookaheads in renaming rules.',
  'cm-invalid-dtRule-captureGroup-namedGroup': 'Dynatrace does not support named capture groups.',
  'cm-invalid-dtRule-captureGroup-greedyComponent': 'Dynatrace does not allow greedy tokens inside of a Capture Group.',
  'cm-invalid-dtRule-captureGroup-groupQuantifier': 'Dynatrace does not support quantified groups here. Please solve this in another way.',
  'cm-invalid-dtRule-captureGroup-quantity': 'Dynatrace allows at most 1 capture group for naming rules.',
  'cm-invalid-dtRule-backreference': 'Dynatrace does not support backreferences.',
  'cm-invalid-dtRule-groupReference': 'Dynatrace does not support using group references.',
  'cm-invalid-dtRule-unknownEscape': 'Dynatrace does not recognize this escaped symbol.',

  // Tooltips for common characters.
  'cm-default': function(e){return {pre: `Matches the string`, cm: e.innerText};},
  'cm-space': 'Matches a SPACE character.',
  'cm-any': 'Matches any character except for newline and carriage return.',
  'cm-alternator': 'Alternates between match sequences.',
  'cm-start-of-string': 'Matches the start of the string/line.',
  'cm-end-of-string': 'Matches the end of the string/line.',
  'cm-unicode-escaped': function(e){ return {
    pre: `Matches this unicode character:`,
    cm: String.fromCharCode(parseInt(e.innerText.substr(2,4), 16))
  };},
  'cm-unicode-range': 'Matches any character in this unicode range.',
  'cm-escaped-symbol': function(e){ return {
    pre: `Escape operator to match the character`,
    cm: e.innerText.slice(1,2)
  };},
  'cm-escaped': function(e){ return {
    pre: `Escape operator to match the character`,
    cm: e.innerText.slice(1,2)
  };},

  // Literal matches.
  'cm-posix': function(e){ return {
    pre: `Matches characters from POSIX class`,
    cm: e.innerText.match(/:(\w+):/)[1]
  };},
  'cm-invalid-syntax-posix': function(e){ return {
    pre: '',
    cm: e.innerText.match(/:(\w+):/)[1],
    post: 'is not a valid POSIX identifier. While it may not impact the regex, it will not match any tokens.'
  };},

  'cm-literal': function(e){ return {
    pre: `Matches all tokens that are specified here: `,
    cm: extractCaptureGroupLabel(e)
  };},
  'cm-literal-inv': function(e){ return {
    pre: `Matches all tokens that are not specified here: `,
    cm: extractCaptureGroupLabel(e)
  };},
  'cm-literal-start': 'Start of literal matches.',
  'cm-literal-end': 'End of literal matches.',
  'cm-range': function(e){ return {pre: `Matches any character in the character range`, cm: e.innerText};},
  'cm-range-inv': function(e){ return {pre: `Matches any character not the character range`, cm: e.innerText};},
  'cm-unicode-range': function(e){ return {pre: `Matches any character between in this unicode range`, cm: e.innerText};},

  // Numeric Quantifiers.
  'cm-numericrange-num': getNumberRangeText,
  'cm-numericrange-delim': getNumberRangeText,
  'cm-numericrange-start': getNumberRangeText,
  'cm-numericrange-end': getNumberRangeText,
  'cm-numericrange-lazy': getNumberRangeText,
  'cm-numericrange-possessive': getNumberRangeText,

  // *, +, ?
  'cm-zero-or-more': "Matches the preceding token zero or more times." ,
  'cm-zero-or-more-possessive': "Matches the preceding token zero or more times, matching as many as possible without releasing any tokens." ,
  'cm-zero-or-more-lazy': "Matches the preceding token zero or more times, matching as few as possible." ,
  'cm-zero-or-more-default': "Matches the preceding token zero or more times." ,
  'cm-one-or-more': "Matches the preceding token one or more times." ,
  'cm-one-or-more-possessive': "Matches the preceding token one or more times, matching as many as possible without releasing any tokens." ,
  'cm-one-or-more-lazy': "Matches the preceding token one or more times, matching as few as possible." ,
  'cm-zero-or-one': "Matches the preceding token zero or one times.",
  'cm-zero-or-one-possessive': "Matches the preceding token zero or one times, and will prioritize matching a token.",


  // POSIX
  'cm-posix': 'Matches any characters from this POSIX group.' ,
  'cm-posix-inv': 'Matches any characters NOT from this POSIX group.' ,
  'cm-posix-digit': function(e){
    return {pre: 'Matches any numeric character. Logically identical to ',
    cm: '[0-9]'};
  },
  'cm-not-posix-digit': 'Matches any non-numeric character.',
  'cm-posix-bound': 'Matches word-boundary characters.',
  'cm-not-posix-bound': 'Matches non-word-boundary characters.',
  'cm-posix-space': 'Matches whitespace characters.',
  'cm-not-posix-space': 'Matches non-whitespace characters.',
  'cm-posix-word': 'Matches word characters.',
  'cm-not-posix-word': 'Matches non-word characters.',


  // Capturegroup stuff
  'cm-captureGroup': function(e){
    let pre, post;
    let specialClass = e.classList.value.match(/cm-((look)(behind|ahead)(-inv)?|atomic|non-capture|namedGroup)/)[0];
    switch(specialClass){
      case "cm-lookbehind":
        pre = `Ensures that the value `;
        post = ` is matched on the left of the rest of the regex.`;
      break;
      case "cm-lookbehind-inv":
      pre = `Ensures that the value `;
      post = ` is not matched on the left of the rest of the regex.`;
      break;
      case "cm-lookahead":
        pre = `Ensures that the value `;
        post = ` is matched on the right of the rest of the regex.`;
      break;
      case "cm-lookahead-inv":
        pre = `Ensures that the value `;
        post = ` is not matched on the right of the rest of the regex.`;
      break;
      case "cm-atomic":
        pre = `Matches`;
        post = "one time without releasing any tokens.";
      break;
      case "cm-non-capture":
        pre =  `Matches`;
        post = "without creating a capture group.";
      break;
      case "cm-namedGroup":
        pre =  `Matches`;
        post = `as a regular Capture Group. You can access it with a named backreference (\k${e.innerText.substr(2)}).`;
      break;
      default:
        pre = "Matches";
        post = "and creates a capture group.";
      break;
    }

    // Default capture group.
    return {pre, cm: extractCaptureGroupLabel(e), post};
  },
  'cm-captureGroup-start': function(e){
    return {
      pre: `Matches the regex`,
      post: `as capture group <b style='color:#97bffb'>${e.classList.value.match(/cm-cgNum-(\d+)/)[1]}</b>.`,
      cm: extractCaptureGroupLabel(e)
    };
  },
  'cm-captureGroup-end': function(e){
    return {
      pre: `Matches the regex`,
      post: `as capture group <b style='color:#97bffb'>${e.classList.value.match(/cm-cgNum-(\d+)/)[1]}</b>.`,
      cm: extractCaptureGroupLabel(e)
    };
  },
};

// The CSS to apply to error items.
const errorStyling = `
  color: #ff0000 !important;
  box-shadow: 0px 0px 4px red !important;
  `;

let errorStylesheet = '';

for(var i = 0; i < Object.keys(messages).length; i++){
  let keys = Object.keys(messages);
  if(keys[i].match(/cm-invalid/)){
    let item = messages[keys[i]];
    errorStylesheet += `.${keys[i]}, `;
  }
}
errorStylesheet += '.error';
// Add the actual CSS part
errorStylesheet += `{${errorStyling}}`;

let ss = document.createElement('style');
ss.id = 'generated-errorStylesheet';
ss.innerHTML = errorStylesheet;

document.head.appendChild(ss);
ss = null;
