FROM node:current

WORKDIR /usr/local/dynex

COPY . .

ENV NODE_ENV=production

RUN apt-get update

# Install git if missing.
RUN apt-get install git -y -qq

RUN npm i
RUN npm i -g pm2

EXPOSE 80/tcp

# Start the pm2 daemon with the application config.
CMD ["pm2-runtime", "ecosystem.config.js"]
