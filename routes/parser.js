var express = require('express');
var router = express.Router();

const PCRE = require('@stephen-riley/pcre2-wasm').default;

(async () => {
  // Initialize WASM PCRE module.
  await PCRE.init();

  console.log("PCRE Version: ", PCRE.version());
})();

let getMatches = async function(regex, text){
  // Initialize timer.
  let startTime = process.hrtime();

  // Compile Regex symbols.
  let rx = new PCRE(regex, 'm');

  // Run the Regex.
  let matches = rx.matchAll(text);

  // Free up WASM memory.
  rx.destroy();

  // End timer.
  let duration = process.hrtime(startTime)[1] / 1000000;

  return {
    results: matches,
    duration,
  };
};

router.post('/pcre', async function(req, res, next) {
  // If we are missing the query parameters, return malformed request.
  if(!req.body.rx || !req.body.rxEval || req.body.rx.length == 0 || req.body.rxEval == 0){
    res.status(400);
    res.send();
    return;
  }
  // Don't evaluate massive payloads.
  if(req.body.rx.length + req.body.rxEval.length > 8192){
    res.status(400);
    res.send();
    return;
  }


  try{
    let matches = await getMatches(req.body.rx, req.body.rxEval);
    // Async timeout solution from https://italonascimento.github.io/applying-a-timeout-to-your-promises/
    const promiseTimeout = function(ms, promise){
      // Create a promise that rejects in <ms> milliseconds
      let timeout = new Promise((resolve, reject) => {
        let id = setTimeout(() => {
          clearTimeout(id);
          res.status(507);
          reject('Timed out in '+ ms + 'ms.');
        }, ms);
      });

      // Return whichever is fastest.
      return Promise.race([
        promise,
        timeout
      ]);
    };
    res.send(await promiseTimeout(200, matches));
  }
  catch(ex){
    if(ex.offset){
      console.info('Invalid Regex at position %s', JSON.stringify(ex.offset));
      res.status(400).send(`Invalid Regex at position ${JSON.stringify(ex.offset)}`);
    }
    else{
      console.log('Regex Evaluation failed.', ex);
      res.status(400).send('Invalid Regex or server error.');
    }
  }
});

module.exports = {
  router,
  match: getMatches
};
