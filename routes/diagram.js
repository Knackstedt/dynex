var express = require('express');
var router = express.Router();

let rr = require("../lib/rr-diagram/regex-to-railroad.js");

router.get('/', function(req, res, next) {
  // If we are missing the query parameters, throw an error.
  if(!req.query.regex){
    res.status(400);
    res.send('Missing required parameter "regex"');
    return;
  }

  try{
    res.send('' + rr.Regex2RailRoadDiagram(req.query.regex, {flavour: 'perl', options: ''}));
  }
  catch(e){
    res.status(200);
    // Occurs when either the regex is not syntactically valid, or some error with the railroad diagram library.
    res.send('<div style="padding: 16px">' + e + '</div>');
  }
});


module.exports = router;
