var express = require('express');
var router = express.Router();

const fs = require('fs');

let version = 'Unset';
try{
  // Simple read to check file version.
  version = fs.readFileSync(__dirname + '/../build.txt');
}
catch(ex){}

router.get('/', function(req, res, next) {
  res.send(version);
});

module.exports = router;
