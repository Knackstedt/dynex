var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');

// Route transactions
var diagram = require('./routes/diagram');
var parser = require('./routes/parser').router;
var version = require('./routes/version');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// Set the directory 'static' to serve static content
app.use(express.static(path.join(__dirname, 'static')));

// Defined these routes as specific API services.
app.use('/diagram', diagram);
app.use('/parser', parser);
app.use('/version', version);

module.exports = app;
