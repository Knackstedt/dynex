DynEx
======

# Description
[DynEx](http://dynatrace.software/)(VPN required) is a regular expression tester and previewer that provides error hinting in order to make regexes easier for Dynatrace without worrying about violating a regex rules. It will automatically warn you about any parts of a regex that would cause Dynatrace to reject it.

## Features
* Potential matches are displayed as you type.
* SVG diagram showing how regular expressions match text
* Hover above matches or regex tokens for details.
* Edit a long or complex regex quickly by viewing the diagram's breakdown

## Build
DynEx uses npm to manage dependencies and docker to build containers.

#### Node Package
  1. Clone the repo and cd into the root directory.
    - `git clone https://gitlab.com/knackstedt/dynex.git`
    - `cd dynex`
  2. Update npm dependencies
    - `npm i`
  3. (Optional) Run tests:
    - `npm test`
  4. Run node.js server and access from Localhost.
    - `npm run start`

  Note: If you encounter issues with Linux, try running as root. Port 80 is blocked by most distributions. You may use tools like authbind or iptables with a custom port configured to avoid this.

#### Docker Image
  1. Clone the docker image
    - `docker pull registry.gitlab.com/knackstedt/dynex:master`
  2. Start the docker image
    - `sudo docker run -p80:80 --name="Dynatrace-Regex-Tester"  registry.gitlab.com/knackstedt/dynex:master`

## Deploy (CICD)

1. Each time the master branch is built, it will automatically create a new docker image. This image can then be pulled from `registry.gitlab.com/knackstedt/dynex`. Please understand that the master branch is the most stable, and dev will probably contain numerous bugs / unreleased features.
2. Pull and run the container. It is set to automatically open on port 80.

## Issues & Feature Requests
Please report issues / feature requests on [GitLab](https://gitlab.com/knackstedt/dynex/issues). Try to follow the template so I can evaluate the issue quickly.

## Contributing
If you would like to contribute, please fork and create a pull request with the new feature and having all tests pass. It will then be reviewed for acceptance.

## License
MIT license. Please view the LICENSE file for details.


## TODO
 - [a-z] renders in diagram incorrectly. Does not show correct box type. - Needs diagram fix.
 - (:) does not render in diagram. - Needs diagram fix.
 - Conditional Rules: Greedy or Lazy are not allowed.
