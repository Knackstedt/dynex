## Descriptive Bug Title

#### Summary of Bug:

#### Steps to reproduce:
1. Load Dynex on IE 4
2. Nothing happens
3. Create issue

#### Additional Details:

Console Details:
```
DynEx running 3.0.0
NodeJS server running null
Npm running 6.13.4
PHP running PHP 7.3.11-1~deb10u1 (cli) (built: Oct 26 2019 14:14:18) ( NTS )
Server is localhost
```
