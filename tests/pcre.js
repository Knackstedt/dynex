var assert = require('assert');
var expressions = require('./expressions');

const PCRE = require('@stephen-riley/pcre2-wasm').default;

// Same code from the parser.
let getMatches = async function(regex, text){
  // Initialize timer.
  let startTime = process.hrtime();

  // Compile Regex symbols.
  let rx = new PCRE(regex);

  // Run the Regex.
  let matches = rx.matchAll(text);

  // Free up WASM memory.
  rx.destroy();

  // End timer.
  let duration = process.hrtime(startTime)[1] / 1000000;

  return {
    results: matches,
    duration,
  };
};


// Run tests for the PHP regex parser.
describe('PCRE', function() {
  let regexItems = Object.keys(expressions);

  // Ensure that the startup completes.
  let init = PCRE.init();

  for(var i = 0; i < regexItems.length; i++){
    let regex = expressions[regexItems[i]].rx;
    let text = expressions[regexItems[i]].text;
    let match = expressions[regexItems[i]].match;

    describe(regexItems[i], function() {
      it('/' + regex + '/', function() {
        let matches = (getMatches(regex, text)).results;
        assert.equal(JSON.stringify(matches), match);
      });
    });
  }
});
