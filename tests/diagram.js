var assert = require('assert');
var expressions = require('./expressions');

let rr = require("../lib/rr-diagram/regex-to-railroad.js");

// Disable it since the current method isn't very accurate...
// describe('Diagram', function() {
//   let regexItems = Object.keys(expressions);
//   for(var i = 0; i < regexItems.length; i++){
//     let regex = expressions[regexItems[i]].rx;
//     let diagramSVG = expressions[regexItems[i]].diagram;
//
//     describe(regexItems[i], function() {
//       it('/' + regex + '/', async function() {
//         let diagram = rr.Regex2RailRoadDiagram(regex, {flavour: 'perl', options: ''});
//         assert.equal(diagram.toString().replace(/\n/g, ''), diagramSVG);
//       });
//     });
//   }
// });
