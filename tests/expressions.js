module.exports = {
  "Basic starting at 0": {
    rx: "sample",
    text: "sample example",
    diagram: '<svg class="railroad-diagram" width="155" height="62" viewBox="0 0 155 62"><g transform="translate(.5 .5)"><path d="M 20 21 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 31h10"></path><g class="literal"><path d="M50 31h0"></path><path d="M104 31h0"></path><rect x="50" y="20" width="54" height="22" rx="10" ry="10"></rect><text x="77" y="35">sample</text></g><path d="M104 31h10"></path><path d="M 114 31 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: "[[[\"sample\",0]]]"
  },
  "Basic starting in middle of string": {
    rx: "sample",
    text: "example sample example",
    diagram: '<svg class="railroad-diagram" width="155" height="62" viewBox="0 0 155 62"><g transform="translate(.5 .5)"><path d="M 20 21 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 31h10"></path><g class="literal"><path d="M50 31h0"></path><path d="M104 31h0"></path><rect x="50" y="20" width="54" height="22" rx="10" ry="10"></rect><text x="77" y="35">sample</text></g><path d="M104 31h10"></path><path d="M 114 31 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: "[[[\"sample\",8]]]"
  },
  "Basic ending at the end of string.": {
    rx: "sample",
    text: "example sample",
    diagram: '<svg class="railroad-diagram" width="155" height="62" viewBox="0 0 155 62"><g transform="translate(.5 .5)"><path d="M 20 21 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 31h10"></path><g class="literal"><path d="M50 31h0"></path><path d="M104 31h0"></path><rect x="50" y="20" width="54" height="22" rx="10" ry="10"></rect><text x="77" y="35">sample</text></g><path d="M104 31h10"></path><path d="M 114 31 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: "[[[\"sample\",8]]]"
  },
  "Basic regex with capture group at start.": {
    rx: '(sam)ple',
    text: "sample",
    diagram: '<svg class="railroad-diagram" width="215" height="102" viewBox="0 0 215 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><g><path d="M40 41h0"></path><path d="M174 41h0"></path><path d="M40 41h10"></path><g class="capture-group group"><path d="M50 41h0"></path><path d="M108 41h0"></path><rect x="50" y="20" width="58" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M50 41h10"></path><path d="M98 41h10"></path><rect x="60" y="30" width="38" height="22" rx="10" ry="10"></rect><text x="79" y="45">sam</text></g><g class="caption"><path d="M47 71h0"></path><path d="M111 71h0"></path><text x="79" y="76">capture 1</text></g></g><path d="M108 41h10"></path><path d="M118 41h10"></path><g class="literal"><path d="M128 41h0"></path><path d="M164 41h0"></path><rect x="128" y="30" width="36" height="22" rx="10" ry="10"></rect><text x="146" y="45">ple</text></g><path d="M164 41h10"></path></g><path d="M 174 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["sample",0]],[["sam",0]]]'
  },
  "Basic regex with capture group at middle.": {
    rx: 's(amp)le',
    text: "sample",
    diagram: '<svg class="railroad-diagram" width="255" height="102" viewBox="0 0 255 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><g><path d="M40 41h0"></path><path d="M214 41h0"></path><path d="M40 41h10"></path><g class="literal"><path d="M50 41h0"></path><path d="M76 41h0"></path><rect x="50" y="30" width="26" height="22" rx="10" ry="10"></rect><text x="63" y="45">s</text></g><path d="M76 41h10"></path><path d="M86 41h10"></path><g class="capture-group group"><path d="M96 41h0"></path><path d="M154 41h0"></path><rect x="96" y="20" width="58" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M96 41h10"></path><path d="M144 41h10"></path><rect x="106" y="30" width="38" height="22" rx="10" ry="10"></rect><text x="125" y="45">amp</text></g><g class="caption"><path d="M93 71h0"></path><path d="M157 71h0"></path><text x="125" y="76">capture 1</text></g></g><path d="M154 41h10"></path><path d="M164 41h10"></path><g class="literal"><path d="M174 41h0"></path><path d="M204 41h0"></path><rect x="174" y="30" width="30" height="22" rx="10" ry="10"></rect><text x="189" y="45">le</text></g><path d="M204 41h10"></path></g><path d="M 214 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["sample",0]],[["amp",1]]]'
  },
  "Basic regex with capture group at end.": {
    rx: 'sam(ple)',
    text: "sample",
    diagram: '<svg class="railroad-diagram" width="215" height="102" viewBox="0 0 215 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><g><path d="M40 41h0"></path><path d="M174 41h0"></path><path d="M40 41h10"></path><g class="literal"><path d="M50 41h0"></path><path d="M88 41h0"></path><rect x="50" y="30" width="38" height="22" rx="10" ry="10"></rect><text x="69" y="45">sam</text></g><path d="M88 41h10"></path><path d="M98 41h10"></path><g class="capture-group group"><path d="M108 41h0"></path><path d="M164 41h0"></path><rect x="108" y="20" width="56" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M108 41h10"></path><path d="M154 41h10"></path><rect x="118" y="30" width="36" height="22" rx="10" ry="10"></rect><text x="136" y="45">ple</text></g><g class="caption"><path d="M104 71h0"></path><path d="M168 71h0"></path><text x="136" y="76">capture 1</text></g></g><path d="M164 41h10"></path></g><path d="M 174 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["sample",0]],[["ple",3]]]'
  },
  "Basic regex where it is only a capture group.": {
    rx: '(sample)',
    text: "sample",
    diagram: '<svg class="railroad-diagram" width="175" height="102" viewBox="0 0 175 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 41h10"></path><g class="capture-group group"><path d="M50 41h0"></path><path d="M124 41h0"></path><rect x="50" y="20" width="74" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M50 41h10"></path><path d="M114 41h10"></path><rect x="60" y="30" width="54" height="22" rx="10" ry="10"></rect><text x="87" y="45">sample</text></g><g class="caption"><path d="M55 71h0"></path><path d="M119 71h0"></path><text x="87" y="76">capture 1</text></g></g><path d="M124 41h10"></path><path d="M 134 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["sample",0]],[["sample",0]]]'
  },
  "Atomic match functionality.":{
    rx: "(?>sample)",
    text: 'sample',
    diagram: '<svg class="railroad-diagram" width="175" height="102" viewBox="0 0 175 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 41h10"></path><g class="rr-atomic-group group positive"><path d="M50 41h0"></path><path d="M124 41h0"></path><rect x="50" y="20" width="74" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M50 41h10"></path><path d="M114 41h10"></path><rect x="60" y="30" width="54" height="22" rx="10" ry="10"></rect><text x="87" y="45">sample</text></g><g title="Atomic Group" class="caption"><path d="M67 71h0"></path><path d="M107 71h0"></path><text x="87" y="76">Atomic</text></g></g><path d="M124 41h10"></path><path d="M 134 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["sample",0]]]'
  },
  "Lookahead functionality.":{
    rx: "(?=sample)",
    text: 'sampleexample',
    diagram: '<svg class="railroad-diagram" width="175" height="102" viewBox="0 0 175 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 41h10"></path><g class="lookahead positive zero-width-assertion group"><path d="M50 41h0"></path><path d="M124 41h0"></path><rect x="50" y="20" width="74" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M50 41h10"></path><path d="M114 41h10"></path><rect x="60" y="30" width="54" height="22" rx="10" ry="10"></rect><text x="87" y="45">sample</text></g><g title="Positive lookahead" class="caption"><path d="M67 71h0"></path><path d="M107 71h0"></path><text x="87" y="76">=> ?</text></g></g><path d="M124 41h10"></path><path d="M 134 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["",0]]]'
  },
  "Lookbehind functionality.":{
    rx: "(?<=sample)",
    text: 'sampleexample',
    diagram: '<svg class="railroad-diagram" width="175" height="102" viewBox="0 0 175 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 41h10"></path><g class="lookbehind positive zero-width-assertion group"><path d="M50 41h0"></path><path d="M124 41h0"></path><rect x="50" y="20" width="74" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M50 41h10"></path><path d="M114 41h10"></path><rect x="60" y="30" width="54" height="22" rx="10" ry="10"></rect><text x="87" y="45">sample</text></g><g title="Positive lookbehind" class="caption"><path d="M67 71h0"></path><path d="M107 71h0"></path><text x="87" y="76">&lt;= ?</text></g></g><path d="M124 41h10"></path><path d="M 134 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["",6]]]'
  },
  "Negative Lookahead functionality.":{
    rx: "(?!sample)",
    text: 'sample',
    diagram: '<svg class="railroad-diagram" width="175" height="102" viewBox="0 0 175 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 41h10"></path><g class="lookahead negative zero-width-assertion group"><path d="M50 41h0"></path><path d="M124 41h0"></path><rect x="50" y="20" width="74" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M50 41h10"></path><path d="M114 41h10"></path><rect x="60" y="30" width="54" height="22" rx="10" ry="10"></rect><text x="87" y="45">sample</text></g><g title="Negative lookahead" class="caption"><path d="M67 71h0"></path><path d="M107 71h0"></path><text x="87" y="76">!> ?</text></g></g><path d="M124 41h10"></path><path d="M 134 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["",1],["",2],["",3],["",4],["",5],["",6]]]'
  },
  "Negative Lookbehind functionality.":{
    rx: "(?<!sample)",
    text: 'sample',
    diagram: '<svg class="railroad-diagram" width="175" height="102" viewBox="0 0 175 102"><g transform="translate(.5 .5)"><path d="M 20 31 v 20 m 10 -20 v 20 m -10 -10 h 20.5"></path><path d="M40 41h10"></path><g class="lookbehind negative zero-width-assertion group"><path d="M50 41h0"></path><path d="M124 41h0"></path><rect x="50" y="20" width="74" height="62" rx="5" ry="5"></rect><g class="literal"><path d="M50 41h10"></path><path d="M114 41h10"></path><rect x="60" y="30" width="54" height="22" rx="10" ry="10"></rect><text x="87" y="45">sample</text></g><g title="Negative lookbehind" class="caption"><path d="M67 71h0"></path><path d="M107 71h0"></path><text x="87" y="76">&lt;! ?</text></g></g><path d="M124 41h10"></path><path d="M 134 41 h 20 m -10 -10 v 20 m 10 -20 v 20"></path></g></svg>',
    phpMatch: '[[["",0],["",1],["",2],["",3],["",4],["",5]]]'
  },
}
